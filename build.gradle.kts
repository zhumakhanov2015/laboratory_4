//import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.0"
    id("org.jetbrains.dokka") version "0.10.0"
    id("org.jmailen.kotlinter") version "3.2.0"
}

group = "org"
version = "1.0-SNAPSHOT"


repositories {
    jcenter()
    maven(url="https://dl.bintray.com/kotlin/dokka")
    mavenCentral()
}


dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.4.20-RC")
    testImplementation(platform("org.junit:junit-bom:5.7.0"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation("org.mockito:mockito-core:1.10.19")
}

tasks.test {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}
tasks.dokka {
    outputFormat = "html"
    outputDirectory = "$buildDir/javadoc"
}
tasks.check {
    dependsOn("installKotlinterPrePushHook")
}
