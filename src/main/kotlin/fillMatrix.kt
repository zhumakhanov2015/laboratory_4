package org

import java.lang.IllegalArgumentException

/**
 * fillMatrix is a function that implements an algorithm that edit matrix by filling it by the algorithm.
 * The matrix may look like this: for example, the matrix 4*4
 * 1, 2, 6, 7,
 * 3, 5, 8, 13,
 * 4, 9, 12, 14,
 * 10, 11, 15, 16
 * @param matrix - filled array with a specific size
 * @param n - matrix dimension
 */
fun fillMatrix(matrix: Matrix, n: Int) {
    if (n <= 0) {
        throw IllegalArgumentException("n shouldn't be lesser or equal zero")
    }
    val matrixBuilder = MatrixBuilder()
    matrixBuilder.buildSize(n)
    IntRange(0, n * n - 1).forEach() {
        matrixBuilder.buildCell(matrix)
    }
}
