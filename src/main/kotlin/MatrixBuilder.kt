package org

/**
 * Builder for matrix
 */
class MatrixBuilder {

    private var n: Int = 0
    private var el: Int = 1
    private var diagonal = 1
    private var diagonalEl = 1
    private var sumAfter = 0
    private var sumBefore = 1

    private fun getRow(): Int {
        return if (diagonal < n) {
            diagonal - diagonalEl
        } else {
            diagonal - diagonalEl - (diagonal - n)
        }
    }

    private fun getColumn(): Int {
        return if (diagonal < n) {
            diagonalEl - 1
        } else {
            diagonalEl - 1 + (diagonal - n)
        }
    }

    /**
     * Build body of matrix with size equal n
     * @param n dimension of the matrix
     */
    fun buildSize(n: Int) {
        this.n = n
    }

    /**
     * Selects a matrix cell based on the number of the current element and writes the value to it
     * @param n - matrix dimension
     * @param matrix - matrix where we will write value
     */
    fun buildCell(matrix: Matrix) {
        if (el > sumBefore) {
            diagonal++
            sumAfter = sumBefore
            if (diagonal <= n) {
                sumBefore = diagonal * (diagonal + 1) / 2
            } else {
                sumBefore += n - diagonal.rem(n)
            }
        }
        diagonalEl = el - sumAfter
        if (diagonal % 2 != 0) {
            matrix.set(getRow(), getColumn(), el)
        } else {
            matrix.set(getColumn(), getRow(), el)
        }
        el++
    }
}
