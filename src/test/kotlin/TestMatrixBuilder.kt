package org

import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock

class TestMatrixBuilder {

    @Test
    fun `Dim 0`() {
        val m = mock(Matrix::class.java)
        try {
            fillMatrix(m, 0)
        } catch (ex: IllegalArgumentException) {
            print(ex.message)
        }
    }

    @Test
    fun `Dim lesser 0`() {
        val m = mock(Matrix::class.java)
        try {
            fillMatrix(m, -1)
        } catch (ex: IllegalArgumentException) {
            print(ex.message)
        }
    }

    @Test
    fun `Matrix 1 x 1`() {
        val m = mock(Matrix::class.java)

        fillMatrix(m, 1)

        Mockito.verify(m).set(0, 0, 1)
    }

    @Test
    fun `Matrix 4 x 4`() {
        val m = mock(Matrix::class.java)

        fillMatrix(m, 4)
//      1, 2, 6, 7,
//      3, 5, 8, 13,
//      4, 9, 12, 14,
//      10, 11, 15, 16
        Mockito.verify(m).set(0, 0, 1)
        Mockito.verify(m).set(0, 1, 2)
        Mockito.verify(m).set(1, 0, 3)
        Mockito.verify(m).set(2, 0, 4)

        Mockito.verify(m).set(1, 1, 5)
        Mockito.verify(m).set(0, 2, 6)
        Mockito.verify(m).set(0, 3, 7)

        Mockito.verify(m).set(1, 2, 8)
        Mockito.verify(m).set(2, 1, 9)
        Mockito.verify(m).set(3, 0, 10)

        Mockito.verify(m).set(3, 1, 11)
        Mockito.verify(m).set(2, 2, 12)

        Mockito.verify(m).set(1, 3, 13)
        Mockito.verify(m).set(2, 3, 14)

        Mockito.verify(m).set(3, 2, 15)
        Mockito.verify(m).set(3, 3, 16)
    }

    @Test
    fun `Matrix 3 x 3`() {
        val m = mock(Matrix::class.java)

        fillMatrix(m, 3)
//      1, 2, 6,
//      3, 5, 7,
//      4, 8, 9,
        Mockito.verify(m).set(0, 0, 1)
        Mockito.verify(m).set(0, 1, 2)
        Mockito.verify(m).set(1, 0, 3)
        Mockito.verify(m).set(2, 0, 4)
        Mockito.verify(m).set(1, 1, 5)
        Mockito.verify(m).set(0, 2, 6)
        Mockito.verify(m).set(1, 2, 7)
        Mockito.verify(m).set(2, 1, 8)
        Mockito.verify(m).set(2, 2, 9)
    }
}
